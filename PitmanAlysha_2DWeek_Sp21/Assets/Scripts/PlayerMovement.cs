﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rb2d; //referrences the Rigidbody2D attached to player parent object, links it with a field variable we named here "rb2d"

    public float runSpeed; //creates a public variable for player run speed that can be altered in engine
    public float jumpForce; //creates a public variable for player jump force
    public SpriteRenderer spriteRender;
    public Animator animator;
    private int count; //tutorial
    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>(); //search and fill our reference to rb2d with Rigidbody2D
        count = (int)0f; //tutorial
        SetCountText();
        winTextObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");
            if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }
    }

    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal"); //get player horizontal location input and return a value based on that input

        rb2d.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rb2d.velocity.y);
        /*set player velocity (speed & direction) to the horizontal input (speed going left or right)
         then ignore whatever the Y value is, we don't need that here*/
        if (rb2d.velocity.x > 0) //making the sprite flip
        {
            spriteRender.flipX = false;
        }
        else if (rb2d.velocity.x < 0)
        {
            spriteRender.flipX = true;
        }
        if (Mathf.Abs(horizontalInput) > 0f)
        {
            animator.SetBool("IsRunning", true);
        }
        else
        {
            animator.SetBool("IsRunning", false);
        }
    }

    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();
        if (count >= 5)
        {
            winTextObject.SetActive(true);
        }
    }

    void Jump()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Enemy"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name); //reload the current scene
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("JumpBox"))
        {
            Destroy(col.transform.parent.gameObject);
        }
        if (col.gameObject.tag.Equals("pickup"))
        {
            Destroy(col.gameObject);
            count = count + 1; //tutorial
            SetCountText();

            //textGameObject.GetComponent<UnityEngine.UI.Text>().text = "text";
        }
    }
}