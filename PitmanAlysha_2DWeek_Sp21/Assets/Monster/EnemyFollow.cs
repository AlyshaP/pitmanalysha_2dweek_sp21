﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour
{
    public Transform player;
    public float moveSpeed = 5f;
   Rigidbody2D Skelrb;
    private Vector2 movement;
    public SpriteRenderer SkelspriteRender;

    // Start is called before the first frame update
    void Start()
    {
        Skelrb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = player.position - transform.position;
        direction.Set(direction.x, 0f, 0f);
        //float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        //rb.rotation = angle;
        direction.Normalize();
        movement = direction;
    }
    private void FixedUpdate()
    {
        moveCharacter(movement);
        if (movement.x > 0) //making the sprite flip
        {
            print("right");
            SkelspriteRender.flipX = false;
        }
        else if (movement.x < 0)
        {
            SkelspriteRender.flipX = true;
            print("left");
        }
    }
    void moveCharacter(Vector2 direction)
    {
        Skelrb.MovePosition((Vector2)transform.position + (direction * moveSpeed * Time.deltaTime));
    }
}